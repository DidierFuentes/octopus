import requests
from bs4 import BeautifulSoup as bs


def scrape(url):
    scraped_text = ''

    if not url or url == '':
        return ''

    page = requests.get(url).content
    soup = bs(page, features='html.parser')

    forms = soup.form
    form_groups = soup(class_='form-group')
    for group in form_groups:
        label = group.label.text.strip()
        input_type = ''
        options = None
        if group.input:
            input_type = group.input['type']

            if input_type == 'checkbox':
                checks = group(class_='checkbox')
                if not (len(checks) == 1 and checks[0].text.strip() == label):
                    options = [child for child in group(class_='checkbox')]
                    
        elif group.textarea:
            input_type = 'long text'

        elif group(name='select'):
            input_type = 'select'
            select = group(name='select')[0]
            options = [o for o in select(name='option')]
        
        result = f'{form_groups.index(group)+1}. {label} [{input_type}]'

        if options:
            for o in options:
                o = ' '.join(o.text.split())
                result += f'\n- {o}'
            
        result += '\n'
        scraped_text += f'\n{result}'

    return scraped_text

def scrape_to_file(url, filename='scraped_text.txt'):
    with open(filename, 'w') as file:
        file.writelines(scrape(url))

if __name__ == '__main__':
    url='http://www.creebanreservas.com.do/Pages/Formulario.aspx?sessionKey=iqdlsh8cxEs7acQ5A1NoEA!!'
    print(scrape(url))